const getUserByUsername = require('../../utils/getUserByUsername')

async function eat({ bot, message, context }) {
  const roomID = message.rid

  // Get the arguments
  let targetUser = context.argumentList[0]
  targetUser = await getUserByUsername(targetUser)

  // Send the message
  message = await bot.sendToRoom(`_Eats ${targetUser.name}!_ :chomp:`, roomID)
}

module.exports = {
  description: 'Eat someone!',
  help: `${process.env.ROCKETCHAT_PREFIX} eat \`username\``,
  call: eat,
}
