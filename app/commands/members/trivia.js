const fetch = require('isomorphic-fetch')

const reactToMessage = require('../../utils/reactToMessage')

function shuffle(array) {
  var currentIndex = array.length,
    temporaryValue,
    randomIndex

  // While there remain elements to shuffle...
  while (0 !== currentIndex) {
    // Pick a remaining element...
    randomIndex = Math.floor(Math.random() * currentIndex)
    currentIndex -= 1

    // And swap it with the current element.
    temporaryValue = array[currentIndex]
    array[currentIndex] = array[randomIndex]
    array[randomIndex] = temporaryValue
  }

  return array
}

const emojiNumbers = [
  'one',
  'two',
  'three',
  'four',
  'five',
  'six',
  'seven',
  'eight',
  'nine',
]

let activeTrivia = []

async function trivia({ bot, message, context }) {
  const roomID = message.rid

  if (activeTrivia.includes(roomID)) {
    await bot.sendToRoom(
      `There's already an active trivia in this room!`,
      roomID,
    )
    return
  } else {
    // Add roomID to active trivias
    activeTrivia.push(roomID)
  }

  fetch('https://opentdb.com/api.php?amount=1')
    .then(async (response) => {
      if (response.status == 200) {
        return response.json()
      }
    })
    .then(async ({ results }) => {
      let { category, question, correct_answer, incorrect_answers } = results[0]

      // Start preparing the question message
      await bot.sendToRoom(
        `:partycat: **TRIVIA** :partycat:\nCategory: *${category}.*\n*You'll have sixty seconds to answer!*\n**Question:** ${question}`,
        roomID,
      )

      // Add the possible answers to the message
      let message = ''
      let allAnswers = shuffle([...incorrect_answers, correct_answer])
      for (let answer in allAnswers) {
        message = message + `:${emojiNumbers[answer]}: ${allAnswers[answer]}\n`
      }

      // Send the message
      let sentMessage = await bot.sendToRoom(message, roomID)

      // Add reactions
      for (let answer in allAnswers) {
        await reactToMessage(emojiNumbers[answer], sentMessage)
      }

      // Start waiting until giving the response
      setTimeout(
        async () => {
          // Send the correct response messages
          await bot.sendToRoom(
            `The correct answer to the question: "${question} was... \n🥁 *drum roll* 🥁 \n `,
            roomID,
          )
          await bot.sendToRoom(`${correct_answer} !!`, roomID)

          // Remove roomID from activeTrivia
          const index = activeTrivia.indexOf(roomID)
          if (index > -1) {
            activeTrivia.splice(index, 1)
          }
        },
        60000,
        'trivia:' + sentMessage._id,
      )
    })

  // Send the message
  // message = await bot.sendToRoom(`_Boops ${targetUser.name}!_`, roomID)
}

module.exports = {
  description: 'Entertaining trivia questions! :partycat:',
  help: `${process.env.ROCKETCHAT_PREFIX} trivia`,
  call: trivia,
}
