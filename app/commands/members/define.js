const { PublicError } = require('../../utils/errors')
const readJSON = require('../../utils/readJSON')
const searchDefinitions = require('../../utils/searchDefinitions')

async function define({ bot, message, context }, minpct = 50) {
  const roomID = message.rid

  let query = message.msg.substring(
    `${context.prefix} ${context.commandName} `.length,
  ).trim()
  let output = ""

  if (query === "") {
    output += "### The MSC Dictionary\n```xml"
    const defFile = 'app/utils/definitions.json'

    // Get definitions
    let definitions
    try {
      definitions = readJSON(defFile)
    } catch (err) {
      console.error(err)
      throw new PublicError("I couldn't access the list of terms! Staff probably need to fix me.")
    }
    let terms = Object.keys(definitions).sort()
    for (let term of terms) {
      output += `\n|   ${definitions[term].name}`
    }
    output + "```"
  } else {
    let terms = searchDefinitions(query, minpct)
    if (terms.length === 0) {
      await bot.sendToRoom("I can't find any matching terms in the dictionary!", roomID)
      return false
    }
    for (let term of terms) {
      if (output !== "") output += "\n\n"
      // else output += "\n"
      output += "*" + term.name + "*"
      if (term.confidence) output += " (confidence: " + term.confidence + "%)"
      output += "\n>" + term.definition
    }
  }
  // If the line below is sending a blank message, you're probably missing a return within an if statement
  await bot.sendToRoom(output, roomID)
  return true
}

module.exports = {
  description: "Search MSC's dictionary of MAP relevant terms",
  help: `${process.env.ROCKETCHAT_PREFIX} define \`term (optional)\``,
  call: define,
}
