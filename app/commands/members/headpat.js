const getUserByUsername = require('../../utils/getUserByUsername')

async function headpat({ bot, message, context }) {
  const roomID = message.rid

  // Get the arguments
  let targetUser = context.argumentList[0]
  targetUser = await getUserByUsername(targetUser)

  // Send the message
  message = await bot.sendToRoom(`_Headpats ${targetUser.name}!_ :headpats:`, roomID)
}

module.exports = {
  description: 'Give someone headpats!',
  help: `${process.env.ROCKETCHAT_PREFIX} headpat \`username\``,
  call: headpat,
}
