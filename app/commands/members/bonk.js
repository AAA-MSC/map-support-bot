const getUserByUsername = require('../../utils/getUserByUsername')

async function bonk({ bot, message, context }) {
  const roomID = message.rid

  // Get the arguments
  let targetUser = context.argumentList[0]
  targetUser = await getUserByUsername(targetUser)

  // Send the message
  message = await bot.sendToRoom(`_Bonks ${targetUser.name}!_ :bonk:`, roomID)
}

module.exports = {
  description: 'Bonk someone!',
  help: `${process.env.ROCKETCHAT_PREFIX} bonk \`username\``,
  call: bonk,
}
