const fetch = require('isomorphic-fetch')
const { PublicError } = require('../../utils/errors')

async function animal({ bot, message, context }) {
  const roomID = message.rid
  let species = context.argumentList[0]

  // Require command to be called in #spam, #comfy-cafe, or a DM with the bot
  if (!['spam', 'comfy-cafe'].includes(message.roomName) && message.roomType !== "d") {
    throw new PublicError('Command needs to be called in #spam or #comfy-cafe or a DM with me')
  }

  if (!species) {
    throw new PublicError("Please include the species")
  }
  species = species.toLowerCase()

  let url
  if (species.startsWith("dog")) url = Math.random() <= 0.5 ? await dogURL() : await dog2URL()
  else if (species.startsWith("cat")) url = await catURL()
  else if (species.startsWith("fox")) url = await foxURL()
  else if (species.startsWith("rac")) url = await raccoonURL()
  else if (species.startsWith("cap")) url = await capybaraURL()
  else throw new PublicError("That species isn't supported")

  let post = await bot.prepareMessage(
    { attachments: [{ image_url: url }] },
    roomID,
  )

  await bot.sendToRoom(post, roomID)
  return true

}

async function dogURL() {
  return fetch('https://dog.ceo/api/breeds/image/random', {
    headers: { Accept: 'application/json' },
  }).then(async (response) => {
    if (response.status == 200) {
      let dog = await response.json()
      return dog.message
    }
  })
}

async function dog2URL() {
  return fetch('https://random.dog/woof.json', {
    headers: { Accept: 'application/json' },
  }).then(async (response) => {
    if (response.status == 200) {
      let dog = await response.json()
      return dog.url
    }
  })
}

async function catURL() {
  return fetch('https://api.thecatapi.com/v1/images/search', {
    headers: { Accept: 'application/json' },
  }).then(async (response) => {
    if (response.status == 200) {
      let cats = await response.json()
      return cats[0].url
    }
  })
}

async function foxURL() {
  return fetch('https://randomfox.ca/floof/', {
    headers: { Accept: 'application/json' },
  }).then(async (response) => {
    if (response.status == 200) {
      let fox = await response.json()
      return fox.image
    }
  })
}

async function raccoonURL() {
  return fetch('https://api.racc.lol/v1/raccoon?json=true', {
    headers: { Accept: 'application/json' },
  }).then(async (response) => {
    if (response.status == 200) {
      let raccoon = await response.json()
      return raccoon.data.url.replace("http://", "https://")
    }
  })
}

async function capybaraURL() {
  return fetch('https://api.capy.lol/v1/capybara?json=true', {
    headers: { Accept: 'application/json' },
  }).then(async (response) => {
    if (response.status == 200) {
      let capybara = await response.json()
      return capybara.data.url.replace("http://", "https://")
    }
  })
}

module.exports = {
  description:
    'Posts a random animal image! Needs to be called in #spam or #comfy-cafe or a DM with me. Currently supported species are dogs, cats, foxes, raccoons, and capybaras',
  help: `${process.env.ROCKETCHAT_PREFIX} animal \`species\``,
  cooldown: 20,
  call: animal,
}
