const mention = require('./mention')

module.exports = {
  ...mention, help: mention.help.replace("mention", "staff")
}
