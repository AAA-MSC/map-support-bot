const { PublicError } = require('../../utils/errors')
const getMessage = require('../../utils/getMessage')

const ruleMsgId = "wK4dmXaH6zc73MQPx"
const ruleEasterEggs = {
    34: "Nice try :unamused2:",
}

async function rule({ bot, message, context }) {
    const roomID = message.rid

    // Determine which rule we are printing
    let ruleNum = Number(context.argumentList[0])
    if (!ruleNum || ruleNum < 0) {
        await bot.sendToRoom("Please enter a valid rule number", roomID)
        return
    }
    if (ruleEasterEggs[ruleNum]) {
        bot.sendToRoom(ruleEasterEggs[ruleNum], roomID)
        return true
    }
    if (ruleNum === 69) ruleNum = 7
    let nextRuleNum = ruleNum + 1

    // Get the full text of the rules as an array with an element for every line
    let rules
    try {
        rules = (await getMessage(ruleMsgId)).msg
    } catch (err) {
        console.error(err)
        throw new PublicError("I couldn't find the message within the rules. Someone probably needs to update my code.", roomID)
    }
    let ruleArr = rules.split("\n")

    // Build the string
    let printing = false
    let found = false
    let ruleTxt = "*Rule " + ruleNum + "*"
    for (ruleLine of ruleArr) {
        if (ruleLine.startsWith(nextRuleNum)) break
        if (!printing && ruleLine.startsWith(ruleNum)) {
            found = true
            printing = true
            ruleLine = ruleLine.slice(3)
        }
        if (printing) {
            if (ruleLine.startsWith("-")) ruleLine = "•" + ruleLine.slice(1)
            ruleTxt += "\n> " + ruleLine
        }
    }

    // Print the string
    if (found) {
        await bot.sendToRoom(ruleTxt, roomID)
        return true
    } else {
        await bot.sendToRoom("That rule does not exist! Visit #rules to see the full list.", roomID)
        return false
    }
}

module.exports = {
    description: 'Get the text of a rule',
    help: `${process.env.ROCKETCHAT_PREFIX} rule \`rule number\``,
    call: rule,
}
