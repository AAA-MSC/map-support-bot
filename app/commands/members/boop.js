const getUserByUsername = require('../../utils/getUserByUsername')

async function boop({ bot, message, context }) {
  const roomID = message.rid

  // Get the arguments
  let targetUser = context.argumentList[0]
  targetUser = await getUserByUsername(targetUser)

  // Send the message
  message = await bot.sendToRoom(`_Boops ${targetUser.name}!_`, roomID)
}

module.exports = {
  description: 'Boop someone!',
  help: `${process.env.ROCKETCHAT_PREFIX} boop \`username\``,
  call: boop,
}
