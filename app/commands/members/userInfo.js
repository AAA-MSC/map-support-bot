const dayjs = require('dayjs')

const getUserByUsername = require('../../utils/getUserByUsername')
const isMinor = require('../../utils/isMinor')
const userHasRole = require('../../utils/userHasRole')
const userHasRoles = require('../../utils/userHasRoles')
const roomInfo = require('../../utils/getRoom')
let roomLists = null
try {
  roomLists = require('../../utils/privateUtils/roomLists')
} catch (err) {
  console.error("Failed to load private utils. Certain commands will not work.")
  console.error(err)
}

const emailsToString = (emails) => {
  let result = ''
  emails.forEach((email) => {
    result = `${result} ${email.address}`
    if (email.verified) {
      result = `${result} ✔️`
    } else {
      result = `${result} ❌`
    }
  })

  return result
}

async function userInfo({ bot, message, context }) {
  const roomID = message.rid

  // Get the targetUser object
  let targetUserName = context.argumentList[0]
  let targetUser = await getUserByUsername(targetUserName, true)

  // Don't show email until we have a flag for it
  // Restrict to mod channels when it is enabled
  // Emails: ${emailsToString(targetUser.emails)}

  let lastLogin = 'Never';
  if (typeof targetUser.lastLogin !== 'undefined') {
    lastLogin = dayjs(targetUser.lastLogin).toString()
  }

  function memberUserInfo() {
    return `
    Name:             ${targetUser.name}
    Username:         ${targetUser.username}
    Roles:            ${targetUser.roles}
    Status:           ${targetUser.statusText ?? ""}
    Created At:       ${dayjs(targetUser.createdAt).toString()}
`}

  async function staffUserInfo() {
    return `
    Type:             ${targetUser.type}
    Name:             ${targetUser.name}
    Username:         ${targetUser.username}
    Roles:            ${targetUser.roles}

    Status:           ${targetUser.status}
    statusConnection: ${targetUser.statusConnection}
          
    Number of Rooms:  ${targetUser.rooms.length}
    utcOffset:        ${targetUser.utcOffset}
    Active:           ${targetUser.active}
    User ID:          ${targetUser._id}

    Status Text:      ${targetUser.statusText ?? ""}

    Last Login:       ${lastLogin}
    Created At:       ${dayjs(targetUser.createdAt).toString()}
`}

  let response = '```';
  let room = await roomInfo({ roomId: roomID });

  if ((roomLists.staffCommand.has(room.name) || message.roomType === 'd') && userHasRoles(await getUserByUsername(message.u.username), ['admin', 'Global Moderator', 'Guide'])) {
    response += await staffUserInfo();
  } else {
    response += memberUserInfo();
  }
  response += '```'
  if (targetUser.bio != undefined) {
    response += `\nBio:`
    targetUser.bio.split("\n").forEach((line) => { response += `\n>${line}` })
  }
  const sentMsg = await bot.sendToRoom(response, roomID)
}

module.exports = {
  description: 'Get information about a specific member.',
  help: `${process.env.ROCKETCHAT_PREFIX} userInfo \`username\``,
  call: userInfo
}
