const fetch = require('isomorphic-fetch')
const { PublicError } = require('../../utils/errors')

let blockedAdvice = new Set([
  111, // You're not as fat as you think you are.
])

async function advice({ bot, message, context }) {
  const roomID = message.rid
  let finished = false

  for (i = 0; i < 5; i++) {
    await fetch('https://api.adviceslip.com/advice')
      .then(async (response) => {
        if (response.status == 200) {
          return response.json()
        }
      })
      .then(async ({ slip }) => {
        if (blockedAdvice.has(slip.id)) {
          console.log("Skipped sending blocked advice: " + slip.advice)
          return
        } else {
          await bot.sendToRoom(slip.advice, roomID)
          finished = true;
        }
      })
    if (finished) break
  }

  if (!finished) {
    throw new PublicError("Could not find any advice. Please try again.")
  }

  return true
}

module.exports = {
  description: 'Get advice!',
  help: `${process.env.ROCKETCHAT_PREFIX} advice`,
  cooldown: 30,
  call: advice,
}
