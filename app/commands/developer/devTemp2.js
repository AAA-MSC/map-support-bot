const getUserByUsername = require('../../utils/getUserByUsername')

const getUsersInRole = require('../../utils/getUsersInRole')
const getUserInfo = require('../../utils/getUserInfo')
const inviteUserToRoom = require('../../utils/inviteUserToRoom')
const getRoom = require('../../utils/getRoom')

async function devTemp2({ bot, message, context }) {
  const roomID = message.rid
  const channel = await getRoom({ roomName: 'minors' })

  let members = await getUsersInRole('minor')
  console.log(members.users.length)
  for (let member of members.users) {
    console.log(member)
    let invite = await inviteUserToRoom(member, channel)
  }
}

module.exports = {
  description: 'Development Script',
  help: `${process.env.ROCKETCHAT_PREFIX}`,
  requireOneOfRoles: ['admin'],
  call: devTemp2,
}
