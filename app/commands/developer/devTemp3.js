const getUserByUsername = require('../../utils/getUserByUsername')

const getUsersInRole = require('../../utils/getUsersInRole')
const getUserInfo = require('../../utils/getUserInfo')
const getMembersInRoom = require('../../utils/getMembersInRoom')
const getRoom = require('../../utils/getRoom')
const removeUserFromRoom = require('../../utils/removeUserFromRoom')

async function devTemp2({ bot, message, context }) {
  const roomID = message.rid
  const channel = await getRoom({ roomName: 'minors' })

  let result = await getMembersInRoom({ roomName: 'minors', count: 0 })
  let members = result.members

  for (let member of members) {
    const userInfo = await getUserInfo(member)
    const { roles } = userInfo

    if (!roles.includes('MAP')) {
      await removeUserFromRoom(userInfo, channel)
    }
  }
}

module.exports = {
  description: 'Development Script',
  help: `${process.env.ROCKETCHAT_PREFIX}`,
  requireOneOfRoles: ['admin'],
  call: devTemp2,
}
