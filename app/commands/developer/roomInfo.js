const dayjs = require('dayjs')

const getRoom = require('../../utils/getRoom')

async function roomInfo({ bot, message, context }) {
  const roomID = message.rid
  let targetRoom = context.argumentList[0]
    ? { roomName: context.argumentList[0] }
    : { roomId: roomID }

  const roomInfo = await getRoom(targetRoom)

  let response = `
        Type:             ${roomInfo.t}
        Name:             ${roomInfo.name}
        Fname:            ${roomInfo.fname}
        ID:               ${roomInfo._id}

        Messages:         ${roomInfo.msgs}
        Users Count:      ${roomInfo.usersCount}

        Broadcast:        ${roomInfo.broadcast}
        Encrypted:        ${roomInfo.encrypted}
        Default:          ${roomInfo.default}
        System Messages:  ${roomInfo.sysMes}

        TS:               ${dayjs(roomInfo.ts).toString()}
        RO:               ${roomInfo.ro}
        Updated At:       ${dayjs(roomInfo._updatedAt).toString()}

        Last Message:
          ID:             ${roomInfo.lastMessage._id}
          Username:       ${roomInfo.lastMessage.u.username}
          Message:        ${roomInfo.lastMessage.msg}
          TS:             ${dayjs(roomInfo.lastMessage.ts).toString()}
          Updated At:     ${dayjs(roomInfo.lastMessage._updatedAt).toString()}
        `

  const sentMsg = await bot.sendToRoom(response, roomID)
}

module.exports = {
  description:
    'Get information about a room. If no roomName is given, it defaults to the room the command was called in.',
  help: `${process.env.ROCKETCHAT_PREFIX} roomInfo \`roomName optional?\``,
  requireOneOfRoles: ['admin'],
  call: roomInfo,
}
