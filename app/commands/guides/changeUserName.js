const { PublicError } = require('../../utils/errors');
const getUserByUsername = require('../../utils/getUserByUsername');
const updateUser = require('../../utils/updateUser');
const isSubordinate = require('../../utils/isSubordinate')

async function changeusername({ bot, message, context }) {
  const roomID = message.rid
  let originalUserName = context.argumentList[0];
  let newUserName = context.argumentList[1];
  let targetUser

  try {
    targetUser = await getUserByUsername(originalUserName);
  } catch (err1) {
    console.log("Getting user failed. Trying again with changename prefix...")
    try {
      originalUserName = "changename-" + originalUserName;
      targetUser = await getUserByUsername(originalUserName);
    } catch (err2) {
      console.error(err1);
      console.error(err2);
      throw new PublicError(err1.message);
    }
  }

  // Ensure the command is being called on a subordinate
  if (!isSubordinate(targetUser, await getUserByUsername(message.u.username))) {
    await bot.sendToRoom("You can only call this command on people you outrank!", roomID,)
    return
  }

  try {
    await updateUser(targetUser, { username: newUserName })
  } catch (err) {
    console.error(err)
    throw new PublicError("Could not change username " + originalUserName + ". Maybe there is already a user with the desired username?")
  }
  await bot.sendToRoom(
    'Updated ' + originalUserName + ' to ' + newUserName,
    roomID,
  )
}

module.exports = {
  description:
    'Change a user\'s username to another ',
  help: `${process.env.ROCKETCHAT_PREFIX} changeusername \`current username\` \`new username\``,
  requireOneOfRoles: ['admin', 'Global Moderator', 'Guide'],
  call: changeusername,
}
