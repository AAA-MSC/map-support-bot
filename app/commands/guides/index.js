const addmap = require('./addMAP')
const onboard = require('./onboard')
const changeusername = require('./changeUserName')
const addally = require('./addAlly')

module.exports = {
  commands: { 'Guide Commands': { addmap, onboard, addally, changeusername } },
}
