const getUserByUsername = require('../../utils/getUserByUsername')
const userHasRole = require('../../utils/userHasRole')
const inviteUserByRoomNames = require('../../utils/inviteUserByRoomNames')
const addUserToRole = require('../../utils/addUserToRole')
let roomLists = null
try {
  roomLists = require('../../utils/privateUtils/roomLists')
} catch (err) {
  console.error("Failed to load private utils. Certain commands will not work.")
  console.error(err)
}
const { PublicError } = require('../../utils/errors')

async function addAlly({ bot, message, context }) {
  const roomID = message.rid
  // Get the targetUser object
  let targetUser = context.argumentList[0]
  targetUser = await getUserByUsername(targetUser)

  // Check if the targetUser has the Non-MAP role
  if (!userHasRole(targetUser, 'Non-MAP')) {
    // User does not have the Non-MAP role
    throw new PublicError(targetUser.name + " does not have the Non-MAP role")
  }
  // Invite the targetUser to default channels
  await bot.sendToRoom(
    'Adding ' + targetUser.name + ' to the ally channels...',
    roomID,
  )
  let inviteErrors = await inviteUserByRoomNames(targetUser, roomLists.onboardedAlly)
  inviteErrors.forEach(async (inviteError) => {
    await bot.sendToRoom(inviteError, context.staffLog,)
  })
  await bot.sendToRoom(
    'Finished inviting ' +
    targetUser.name +
    ' to the ally channels! :D',
    roomID,
  )

  if (!userHasRole(targetUser, 'minor') && !userHasRole(targetUser, 'DM')) {
    await bot.sendToRoom(
      'Adding DM role to ' + targetUser.name,
      roomID,
    )
    let role = await addUserToRole(targetUser, 'DM')
    if (role.name == 'DM') {
      await bot.sendToRoom(
        'Added DM role to ' + targetUser.name + ". All done!",
        roomID,
      )
    }
  }
}

module.exports = {
  description:
    'Add a non-map to the appropriate channels for an ally.',
  help: `${process.env.ROCKETCHAT_PREFIX} addAlly \`username\``,
  requireOneOfRoles: ['admin', 'Global Moderator', 'Guide'],
  call: addAlly,
}