const getUserByUsername = require('../../utils/getUserByUsername')
const userHasRole = require('../../utils/userHasRole')
let roomLists = null
try {
  roomLists = require('../../utils/privateUtils/roomLists')
} catch (err) {
  console.error("Failed to load private utils. Certain commands will not work.")
  console.error(err)
}
const removeUserByRoomNames = require('../../utils/removeUserByRoomNames')
const inviteUserByRoomNames = require('../../utils/inviteUserByRoomNames')
const { PublicError } = require('../../utils/errors')

async function onboard({ bot, message, context }) {
  const roomID = message.rid
  const roomName = message.roomName

  // Require command to be called in #--pre-screening--
  if (roomName !== 'pre-screening') {
    await bot.sendToRoom(
      'Command needs to be called in #pre-screening',
      roomID,
    )
    return
  }

  // Get the targetUser object
  let targetUser = context.argumentList[0]
  targetUser = await getUserByUsername(targetUser, true)

  // User can't have more than one role
  // this should ensure that the user only has the user role.
  // An attempt was made to compare:
  // targetUser.roles !== ['user']
  // But it did not work for some reason.

  // TODO: Look for specific roles rather than amount of roles
  if (targetUser.roles.length > 2) {
    throw new PublicError("This command cannot be called on a user with any other role than the `user` and either `DM` or `minor` roles")
  }

  // Invite the targetUser to channels
  await bot.sendToRoom(
    'Inviting ' + targetUser.name + ' to the onboarding channels...',
    roomID,
  )
  let inviteErrors = await inviteUserByRoomNames(targetUser, roomLists.onboardingMAP)
  inviteErrors.forEach(async (inviteError) => {
    await bot.sendToRoom(inviteError, context.staffLog,)
  })
  await bot.sendToRoom(
    'Finished inviting ' + targetUser.name + ' to the onboarding channels! :D',
    roomID,
  )

  // Remove the targetUser from pre-screening
  await bot.sendToRoom('Removing ' + targetUser.name + ' from #pre-screening ...', roomID)
  kickErrors = await removeUserByRoomNames(targetUser, ["pre-screening"])
  kickErrors.forEach(async (kickError) => {
    await bot.sendToRoom(kickError, context.staffLog,)
  })

  await bot.sendToRoom('User has been moved to onboarding! :D', roomID)
}

module.exports = {
  description: 'Onboard a member, moving them unto the onboarding stage',
  help: `${process.env.ROCKETCHAT_PREFIX} onboard \`username\``,
  requireOneOfRoles: ['admin', 'Global Moderator', 'Guide'],
  call: onboard,
}