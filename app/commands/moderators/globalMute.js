const getUserByUsername = require('../../utils/getUserByUsername')
const slashCommandUserParam = require('../../utils/slashCommandUserParam')
const isSubordinate = require('../../utils/isSubordinate')

async function globalMute({ bot, message, context }) {
  const roomID = message.rid

  // Get the targetUser object
  let targetUser = context.argumentList[0]
  targetUser = await getUserByUsername(targetUser, true)

  // Ensure the command is being called on a subordinate
  if (!isSubordinate(targetUser, await getUserByUsername(message.u.username))) {
    await bot.sendToRoom("You can only call this command on people you outrank!", roomID,)
    return
  }

  let userRooms = targetUser.rooms.filter((room) => room.t === 'c' || room.t === 'p')

  await bot.sendToRoom(`Attempting to mute ${targetUser.name} in every channel...`, roomID)
  for (room of userRooms) {
    await slashCommandUserParam('mute', room, targetUser);
  }

  response = `${targetUser.name} has been muted in every channel I can reach. Some private channels may require manual muting.`;
  await bot.sendToRoom(response, roomID)
}

module.exports = {
  description: 'Mute a user in all channels that the bot can reach.',
  help: `${process.env.ROCKETCHAT_PREFIX} globalMute \`username\``,
  requireOneOfRoles: ['admin', 'Global Moderator'],
  call: globalMute
}
