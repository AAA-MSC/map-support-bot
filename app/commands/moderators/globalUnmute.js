const getUserByUsername = require('../../utils/getUserByUsername')
const slashCommandUserParam = require('../../utils/slashCommandUserParam')

async function globalUnmute({ bot, message, context }) {
  const roomID = message.rid

  // Get the targetUser object
  let targetUser = context.argumentList[0]
  targetUser = await getUserByUsername(targetUser, true)
  let userRooms = targetUser.rooms.filter((room) => room.t === 'c' || room.t === 'p')

  await bot.sendToRoom(`Attempting to unmute ${targetUser.name} in every channel...`, roomID)
  for (room of userRooms) {
    await slashCommandUserParam('unmute', room, targetUser);
  }

  response = `${targetUser.name} has been unmuted in every channel I can reach. Any channel where you had to manually mute will also require a manual unmute.`;

  await bot.sendToRoom(response, roomID)
}

module.exports = {
  description: 'Unmute a user in all channels that the bot can reach.',
  help: `${process.env.ROCKETCHAT_PREFIX} globalUnmute \`username\``,
  requireOneOfRoles: ['admin', 'Global Moderator'],
  call: globalUnmute
}
