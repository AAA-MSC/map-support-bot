const getUserByUsername = require('../utils/getUserByUsername')
const userHasRoles = require('../utils/userHasRoles')

async function help({ bot, message, context }) {
  const roomID = message.rid

  try {
    const user = await getUserByUsername(message.u.username)

    let requestedCommand = context.argumentList[0]

    if (requestedCommand === undefined) {
      let message = ''
      let commandsPortionOfMessage = ''

      for (commandGroup of Object.keys(context.commands)) {
        // Reset commandsPortionOfMessage
        commandsPortionOfMessage = ''

        for (commandName of Object.keys(context.commands[commandGroup])) {
          let command = context.commands[commandGroup][commandName]
          if (
            // Empty list? or...
            command.requireOneOfRoles == false ||
            // If not, then Undefined list? or...
            command.requireOneOfRoles === undefined ||
            // If not, then does user have one of the roles?
            userHasRoles(user, command.requireOneOfRoles)
          ) {
            commandsPortionOfMessage =
              commandsPortionOfMessage +
              `- *${commandName}*: ${command.description}\n`
          }
        }

        // If the the group had any commands available to the user,
        // add them to the message
        if (commandsPortionOfMessage !== '') {
          message = message + `**${commandGroup}:**\n`
          message = message + commandsPortionOfMessage
        }
      }

      message =
        message +
        `\nType "${context.prefix} help \`some command\`" for more information about a specific command.`
      await bot.sendToRoom(message, roomID)
    } else {
      let allCommands = {}
      for (commandGroup of Object.keys(context.commands)) {
        for (commandName of Object.keys(context.commands[commandGroup]))
          allCommands[commandName] = context.commands[commandGroup][commandName]
      }

      console.log(allCommands)

      try {
        let command = allCommands[requestedCommand]
        let message = `${command.description}
Usage: ${command.help}
            `
        await bot.sendToRoom(message, roomID)
      } catch (error) {
        const sentMsg = await bot.sendToRoom(
          `Unknown command 😕. Try \`${context.prefix} ${context.commandName}\` to see all the available commands!`,
          roomID,
        )
      }
    }
  } catch (err) {
    console.error(err)
    await bot.sendToRoom(
      'Command error',
      roomID
    )
  }
}

module.exports = help
