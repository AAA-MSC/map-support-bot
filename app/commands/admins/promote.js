const getUserByUsername = require('../../utils/getUserByUsername')
const addUserToRole = require('../../utils/addUserToRole')
const userHasRole = require('../../utils/userHasRole')
const userHasRoles = require('../../utils/userHasRoles')
const roleNames = require('../../utils/roleNames')
let roomLists = null
try {
  roomLists = require('../../utils/privateUtils/roomLists')
} catch (err) {
  console.error("Failed to load private utils. Certain commands will not work.")
  console.error(err)
}
const inviteUserByRoomNames = require('../../utils/inviteUserByRoomNames')
const { PublicError } = require('../../utils/errors')
const removeUserFromRoles = require('../../utils/removeUserFromRoles')

const guideRole = "Guide"
const modRole = "Global Moderator"
const adminRole = "admin"
const staffRoles = [guideRole, modRole, adminRole]

async function promote({ bot, message, context }) {
  const roomID = message.rid

  if (context.argumentList[1] !== "to") {
    throw new PublicError("Incorrect format. Please format the command as '!map promote `username` to `role`'")
  }

  // Get the targetUser object
  let targetUserName = context.argumentList[0]
  targetUser = await getUserByUsername(targetUserName, true)

  // Make sure targetUser has the MAP role
  if (!userHasRole(targetUser, "MAP")) throw new PublicError("You can only promote MAPped members to staff!")

  // Determine what level the user is being promoted to and check whether they qualify
  let targetRole = context.argumentList[2].toLowerCase()
  if (roleNames.GUIDE.has(targetRole)) {
    // Mods and admins cannot be promoted to guides
    if (userHasRoles(targetUser, [modRole, adminRole])) throw new PublicError("You can only promote regular users to guides!")
    targetRole = guideRole
  } else if (roleNames.MOD.has(targetRole)) {
    // Admins and non-guides cannot be promoted to mods
    if (!userHasRoles(targetUser, [guideRole, modRole]) || userHasRole(targetUser, adminRole)) throw new PublicError("You can only promote guides to moderators!")
    targetRole = modRole
  } else if (roleNames.ADMIN.has(targetRole)) {
    // Non-mods cannot be promoted to admins
    if (!userHasRoles(targetUser, [modRole, adminRole])) throw new PublicError("You can only promote moderators to admins!")
    targetRole = adminRole
  } else {
    throw new PublicError(`Could not find the role ${targetRole}`)
  }
  let priorRole = staffRoles[staffRoles.indexOf(targetRole) - 1]

  // Check if the targetUser is already the desired level of staff
  let readding = false
  if (userHasRole(targetUser, targetRole)) {
    await bot.sendToRoom(`${targetUser.name} already has the desired rank, but I'll make sure they're in all the staff channels for that rank.`, roomID)
    readding = true
  } else {
    await bot.sendToRoom(`Giving ${targetUser.name} the ${targetRole} role...`, roomID)
    await addUserToRole(targetUser, targetRole)
    if (priorRole) {
      targetUser = await getUserByUsername(targetUser.username)
      await removeUserFromRoles(targetUser, [priorRole])
    }
    await bot.sendToRoom(`Inviting ${targetUser.name} to the rooms for their new rank...`, roomID)
  }

  // Determine what rooms the user should be added to
  let roomsToInvite = []
  // Guides and all readds get added to guide channels
  if (targetRole === guideRole || readding == true) roomsToInvite = roomsToInvite.concat(roomLists.guide)
  // Admin readds and mods get added to mod channels
  if (targetRole === modRole || (readding == true && targetRole === adminRole)) roomsToInvite = roomsToInvite.concat(roomLists.moderator)
  // Admins get added to admin channels
  if (targetRole === adminRole) roomsToInvite = roomsToInvite.concat(roomLists.admin)

  //Invite user to the list of rooms
  let inviteErrors = await inviteUserByRoomNames(targetUser, roomsToInvite)
  inviteErrors.forEach(async (inviteError) => {
    await bot.sendToRoom(inviteError, context.staffLog,)
  })
  await bot.sendToRoom('Finished! Remember to add them to the forum or update their permissions.', roomID,)
}

module.exports = {
  description:
    'Promote a member to the next staff level and add them to the appropriate channels.',
  help: `${process.env.ROCKETCHAT_PREFIX} promote \`username\` to \`desired role\``,
  requireOneOfRoles: ['admin'],
  call: promote,
}