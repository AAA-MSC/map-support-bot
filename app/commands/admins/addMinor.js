const getUserByUsername = require('../../utils/getUserByUsername')
const writeJSON = require('../../utils/writeJSON')
const readJSON = require('../../utils/readJSON')

const updateMinors = minor => {
  let json = readJSON('./minors.json')
  json[minor._id] = {
    name: minor.name,
    username: minor.username,
    emails: minor.emails,
  }
  writeJSON('./minors.json', json)
}

async function addMinor({ bot, message, context }) {
  const roomID = message.rid
  try {
    // Get the targetUser object
    let targetUser = context.argumentList[0]
    targetUser = await getUserByUsername(targetUser)

    updateMinors(targetUser)

    await bot.sendToRoom(
      `${targetUser.username} has been added to minors file`,
      roomID,
    )
  } catch (err) {
    console.error(err)
    await bot.sendToRoom(
      'Command error occurred',
      roomID
    )
  }
}

module.exports = {
  description: 'Mark a user as a minor.',
  help: `${process.env.ROCKETCHAT_PREFIX} addMinor \`username\``,
  requireOneOfRoles: ['admin'],
  call: addMinor,
}
