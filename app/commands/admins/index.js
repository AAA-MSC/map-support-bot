const addawa = require('./addAWA')
const addminor = require('./addMinor')
const announce = require('./announce')
const deleteminor = require('./deleteMinor')
const demote = require('./demote')
const getdms = require('./getDMs')
const giverole = require('./giveRole')
const invitemaps = require('./inviteMAPs')
const inviteally = require('./inviteAlly')
const promote = require('./promote')
const redefine = require('./redefine')
const listStaffMembers = require('./listStaffMembers')

module.exports = {
  commands: { 'Administrator Commands': { addawa, giverole, announce, invitemaps, inviteally, demote, promote, redefine, listStaffMembers } },
}
