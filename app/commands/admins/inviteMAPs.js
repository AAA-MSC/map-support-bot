const deleteMessage = require('../../utils/deleteMessage')
const addUser = require('../../utils/addUser');

async function inviteMAPs({ bot, message, context }) {
    const roomID = message.rid

    // Ensure the command is called in DMs
    if (message.roomType === 'd' || message.roomName === "invites-bot") {
        await bot.sendToRoom("Inviting " + Math.ceil(context.argumentList.length / 3) + " members...", roomID,)
        let output = "";
        let failure = false;

        while (context.argumentList.length > 0) {
            // Get the arguments
            let number = context.argumentList.shift();
            let name = "changename-" + number;
            let age = context.argumentList.shift();
            let email = context.argumentList.shift();

            output += "\n*" + number + ": *";
            try {
                await addUser(name, email, true, (age < 18 ? ["minor"] : []))
                output += "Success";
            } catch (err) {
                console.error(err);
                output += "Failed!";
                failure = true;
            }

        }
        if (failure) output += "\n@all One or more users could not be created. Try creating the affected users manually to see information about the error."
        await bot.sendToRoom("#### inviteMAPs Report" + output, roomID,)
    } else {
        // Delete the user's message
        await deleteMessage(roomID, message._id)
        await bot.sendToRoom(
            'For privacy reasons, this command can only be called in DMs',
            roomID,
        )
        return
    }
}

module.exports = {
    description: "Add new members while accounting for the minor role",
    help: `${process.env.ROCKETCHAT_PREFIX} inviteMAPs \`changename number\` \`age\` \`email\` (repeated for each user)`,
    requireOneOfRoles: ['admin', 'bot'],
    call: inviteMAPs,
}
