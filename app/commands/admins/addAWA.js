const getUserByUsername = require('../../utils/getUserByUsername')
const addUserToRole = require('../../utils/addUserToRole')
const userHasRole = require('../../utils/userHasRole')
const inviteUserByRoomNames = require('../../utils/inviteUserByRoomNames')
const { PublicError } = require('../../utils/errors')
let roomLists = null
try {
  roomLists = require('../../utils/privateUtils/roomLists')
} catch (err) {
  console.error("Failed to load private utils. Certain commands will not work.")
  console.error(err)
}

async function addAWA({ bot, message, context }) {
  const roomID = message.rid

  // Get the targetUser object
  let targetUser = context.argumentList[0]
  targetUser = await getUserByUsername(targetUser)
  let roomsToInvite

  if (!userHasRole(targetUser, 'Non-MAP')) {
    // User does not have the Non-MAP role, no need to do anything else
    throw new PublicError(targetUser.name + " doesn't have the Non-MAP role")
  }

  // Check if the targetUser does not already have the AWA role
  if (!userHasRole(targetUser, 'AWA')) {
    // Add the AWA role to targetUser
    await bot.sendToRoom(
      'Giving ' + targetUser.name + ' the AWA role...',
      roomID,
    )
    await addUserToRole(targetUser, 'AWA')

    roomsToInvite = roomLists.AWA
  } else {
    await bot.sendToRoom(
      targetUser.name + " already has the AWA role. I'll go ahead and re-add them to every channel an AWA should be able to access.",
      roomID,
    )
    roomsToInvite = roomLists.defaultAlly.concat(roomLists.onboardedAlly.concat(roomLists.AWA));
  }

  //Invite user to the relevant channels
  await bot.sendToRoom(
    'Inviting ' + targetUser.name + ' to the AWA channels...',
    roomID,
  )
  let inviteErrors = await inviteUserByRoomNames(targetUser, roomsToInvite)
  inviteErrors.forEach(async (inviteError) => {
    await bot.sendToRoom(inviteError, context.staffLog,)
  })
  await bot.sendToRoom(
    'Finished inviting ' + targetUser.name + ' to the AWA channels!',
    roomID,
  )
}

module.exports = {
  description:
    'Give a non-map the AWA (Allies-With-Access) role and invite them to the appropriate channels.',
  help: `${process.env.ROCKETCHAT_PREFIX} addAWA \`username\``,
  requireOneOfRoles: ['admin'],
  call: addAWA,
}
