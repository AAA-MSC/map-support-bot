const getUserByUsername = require('../../utils/getUserByUsername')
const writeJSON = require('../../utils/writeJSON')
const readJSON = require('../../utils/readJSON')

const updateMinors = minor => {
  let json = readJSON('./minors.json')
  delete json[minor._id]
  writeJSON('./minors.json', json)
}

async function deleteMinor({ bot, message, context }) {
  const roomID = message.rid

  try {
    // Get the targetUser object
    let targetUser = context.argumentList[0]
    targetUser = await getUserByUsername(targetUser)

    updateMinors(targetUser)

    await bot.sendToRoom(
      `${targetUser.username} has been removed from minors file`,
      roomID,
    )
  } catch (err) {
    console.error(err)
    await bot.sendToRoom(
      'Command error occurred',
      roomID
    )
  }
}

module.exports = {
  description: 'Unmark a user as a minor.',
  help: `${process.env.ROCKETCHAT_PREFIX} deleteMinor \`username\``,
  requireOneOfRoles: ['admin'],
  call: deleteMinor,
}
