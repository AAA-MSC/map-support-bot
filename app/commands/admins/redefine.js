const { PublicError } = require("../../utils/errors")
const findWord = require("../../utils/findWord")
const readJSON = require("../../utils/readJSON")
const writeJSON = require("../../utils/writeJSON")

const defFile = 'app/utils/definitions.json'

async function redefine({ bot, message, context }) {
  const roomID = message.rid

  // Parse the user's input to get the data we need
  let messageArr = message.msg.split("\n", 2)
  if (messageArr.length < 2 || messageArr[1].trim() == "") throw new PublicError("Inproperly formatted input! Run `!map help redefine` to see the correct format.")
  let key = messageArr[0].substring(
    `${context.prefix} ${context.commandName} `.length,
  )
  let newDef = messageArr[1].trim()

  let old = findWord(key)

  let definitions
  try {
    definitions = readJSON(defFile)
  } catch (err) {
    console.error(err)
    throw new PublicError("I couldn't access the list of definitions. Staff probably need to fix me.")
  }

  definitions[old.key]["definition"] = newDef

  // Create the new term
  console.log("Updating the definition of term '" + old.name + "' with key '" + old.key + "'...")
  writeJSON(defFile, definitions)
  let output = "The definition has been updated. Here's what was changed:\n\n"
  output += "*Old Definition: *\n>" + old.definition + "\n\n"
  output += "*New Definition: *\n>" + definitions[old.key]["definition"]
  bot.sendToRoom(output, roomID)

  return true
}

module.exports = {
  description: "Change the definition of a word in the bot's dictionary",
  help: `This command has required line breaks

${process.env.ROCKETCHAT_PREFIX} redefine \`word\`
\`new definition\` - This may not have any line breaks.`,
  requireOneOfRoles: ['admin'],
  call: redefine,
}
