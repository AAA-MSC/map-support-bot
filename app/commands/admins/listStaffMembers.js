const getUsersInRole = require("../../utils/getUsersInRole");
const roomInfo = require('../../utils/getRoom')
let roomLists = null
try {
    roomLists = require('../../utils/privateUtils/roomLists')
} catch (err) {
    console.error("Failed to load private utils. Certain commands will not work.")
    console.error(err)
}

async function listStaffMembers({ bot, message, context }) {
    const roomID = message.rid
    let room = await roomInfo({ roomId: roomID });

    if (message.roomType === 'd' || roomLists.staffCommand.has(room.name)) {

        await bot.sendToRoom(`Users in the Admin Role are:`, roomID)

        let adminResult = await getUsersInRole("admin")

        for (var user of adminResult) {
            await bot.sendToRoom(`@${user.username}`, roomID)
        }

        await bot.sendToRoom(`Users in the Moderator Role are:`, roomID)
        let modResult = await getUsersInRole("Global Moderator")

        for (var user of modResult) {
            await bot.sendToRoom(`@${user.username}`, roomID)
        }

        await bot.sendToRoom(`Users in the Guide Role are:`, roomID)
        let guideResult = await getUsersInRole("Guide")

        for (var user of guideResult) {
            await bot.sendToRoom(`@${user.username}`, roomID)
        }

    } else {
        await bot.sendToRoom("This command must be called in a DM with the bot or a staff command channel.", roomID)
    }
    
}

module.exports = {
    description: 'List usernames of each staff member at each rank',
    help: `${process.env.ROCKETCHAT_PREFIX} listStaffMembers`,
    requireOneOfRoles: ['admin', 'bot'],
    call: listStaffMembers,
}