const { api } = require('@rocket.chat/sdk')

const getDirectMessages = async (options = undefined, options2 = undefined) => {
  try {
    let allDMs = await api.get('im.list.everyone', options)
    let result = await api.get('im.messages.others', {
      roomId: allDMs.ims[0]._id,
      ...options2,
    })
    if (result && result.success) {
      return result
    } else {
      console.error(result)
    }
  } catch (err) {
    console.error(err)
    return err
  }
}

module.exports = getDirectMessages
