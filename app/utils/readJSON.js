const fs = require('fs')

const readJSON = file => {
  try {
    const jsonString = fs.readFileSync(file)
    const jsonParsed = JSON.parse(jsonString)
    return jsonParsed
  } catch (error) {
    console.error(error)
    return
  }
}

module.exports = readJSON
