const { api } = require('@rocket.chat/sdk')
const { PublicError } = require('./errors')

/**
 * Updates one or more fields on a user's account.
 * @param {import('@rocket.chat/sdk/dist/utils/interfaces').IUserAPI} user - The user to be updated
 * @param {object} data - The data field of the user.update API call ({@link https://developer.rocket.chat/reference/api/rest-api/endpoints/user-management/users-endpoints/update-user})
 * @returns {import('@rocket.chat/sdk/dist/utils/interfaces').IUserAPI} The updated user object
 * @throws Throws an error if the user could not be updated or an API error occurred
 */
const updateUser = async (user, data) => {
  console.log("Updating " + user.name + "...")
  result = await api.post('users.update', {
    userId: user._id,
    data: data,
  })

  if (result && result.success) {
    return result.user
  } else {
    console.error(result)
    throw new PublicError("Could not update user " + user.name)
  }
}

module.exports = updateUser