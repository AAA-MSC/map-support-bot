const getRoomByRoomName = require('./getRoomByRoomName')

/**
 * Gets a set of room objects by the names of the rooms. If you are planning to invite the user to these rooms, use the inviteUserByRoomNames util instead.
 * @param {string[]} roomNames - The names of the rooms, labeled "Name" in the output of the roomInfo dev command
 * @returns {import('@rocket.chat/sdk/dist/utils/interfaces').IRoomAPI[]} A list room objects associated with the names passed in. Will exclude rooms that resulted in an error.
 * @deprecated Use the getRoomByRoom name util inside a try-catch block inside a for loop to get customizable error handling while building a list of rooms.
 */
const getRoomsByRoomName = async (roomNames) => {
  console.log("Getting rooms with names " + roomNames.toString() + "...")
  let rooms = []
  for (let roomName of roomNames) {
    if (roomName !== undefined) {
      try {
        rooms.push(await getRoomByRoomName(roomName))
      } catch (err) {
        console.error(err)
      }
    }
  }
  return rooms
}

module.exports = getRoomsByRoomName