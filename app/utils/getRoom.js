const { api } = require('@rocket.chat/sdk')

const getRoom = async (room) => {
  try {
    let result = await api.get('rooms.info', { ...room })
    if (result && result.success) {
      return result.room
    } else {
      console.error(result)
    }
  } catch (err) {
    console.error(err)
    return err
  }
}

module.exports = getRoom
