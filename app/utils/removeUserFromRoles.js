const { api } = require('@rocket.chat/sdk')
const { StaffError } = require('./errors')

/**
 * Removes a user from multiple roles at once.
 * @param {import('@rocket.chat/sdk/dist/utils/interfaces').IUserAPI} user - The user who should be removed from the roles
 * @param {string[]} rolesToRemove - A list of roles that the user should be removed from
 * @param {string} roomId - Not used. Defaults to undefined.
 * @returns {import('@rocket.chat/sdk/dist/utils/interfaces').IUserAPI} The updated user object
 * @throws Throws an error if the user could not be removed from the roles or an API error occurred
 */
const removeUserFromRoles = async (user, rolesToRemove, roomId = undefined) => {
  console.log("Removing " + user.name + " from roles " + rolesToRemove.toString() + "...")
  let newRoles = user.roles.filter((role) => !rolesToRemove.includes(role))
  console.log(newRoles)
  let result = await api.post('users.update', {
    userId: user._id,
    data: {
      roles: newRoles,
    },
  })
  if (result && result.success) {
    return result.user
  } else {
    console.error(result)
    throw new StaffError("Failed to remove " + user.name + " from roles: " + rolesToRemove.toString())
  }
}

module.exports = removeUserFromRoles
