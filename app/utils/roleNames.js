module.exports = {
    GUIDE: new Set(['guide', 'guides']),
    MOD: new Set(['moderator', 'moderators', 'mod', 'mods', 'global moderator']),
    ADMIN: new Set(['admin', 'admins', 'administrator', 'administrators']),
    ALL: new Set(['staff', 'all']),
}