const { api } = require('@rocket.chat/sdk')

/**
 * Get a list of the users in a certain role.
 * @param {string} role - The name of the role you want a list of users from
 * @param {string} roomId - The room where you want to look for members with the role (defaults to undefined)
 * @returns A list of users with the role. List will be empty if there are no users in the role, if the role does not exist, or if an API error occurred.
 */
const getUsersInRole = async (role, roomId = undefined) => {
  console.log("Getting users in role " + role + "...")
  let result = await api.get('roles.getUsersInRole', { role, roomId, count: 0 })
  if (result && result.success) {
    return result.users
  } else {
    console.error("Failed to get users in the " + role + " role. Result starts on next line:")
    console.error(result)
    console.log("Returning an empty list in attempt to save command")
    return []
  }
}

module.exports = getUsersInRole
