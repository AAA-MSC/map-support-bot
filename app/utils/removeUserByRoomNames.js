const getUserByUsername = require('./getUserByUsername')
const removeUserFromRoom = require('./removeUserFromRoom')

/**
 * Removes a user from all of the rooms in a list. Will not affect DMs or private rooms that the bot is not a member of.
 * @param {import('@rocket.chat/sdk/dist/utils/interfaces').IUserAPI} user - The user to be removed from the rooms
 * @param {string[]} rooms - The list of rooms to remove the user from
 * @returns {string[]} A list of errors encountered while removing users from rooms
 */
const removeUserByRoomNames = async (user, rooms) => {
    removalRooms = new Set(rooms)
    errors = []

    // Find out which rooms the user is in
    if (!user.rooms) {
        user = await getUserByUsername(user.username, true)
    }

    // Find out which rooms the bot is in
    bot = await getUserByUsername(process.env.ROCKETCHAT_USER, true)
    botRooms = new Set(bot.rooms.map(room => { return room.name }))

    for (let room of user.rooms) {
        if (removalRooms.has(room.name)) {
            if (room.t === "p" && !botRooms.has(room.name)) {
                errors.push("Error while removing " + user.name + " from #" + room.name + ": That's a private room that I'm not in.")
            } else {
                try {
                    await removeUserFromRoom(user, room)
                } catch (err) {
                    console.error(err)
                    errors.push("Error while removing " + user.name + " from #" + room.name + ": " + err.message)
                }
            }
        }
    }
    return errors
}

module.exports = removeUserByRoomNames