const { api } = require('@rocket.chat/sdk')
const { StaffError } = require('./errors')

/**
 * Gets a room object using the name of the room. If you are planning to invite the user to this room, use the inviteUserByRoomNames util instead.
 * @param {string} roomName - Labeled "Name" in the output of the roomInfo dev command
 * @returns {import('@rocket.chat/sdk/dist/utils/interfaces').IRoomAPI} The room object with the name passed in
 * @throws Throws an error if the room could not be found or an API error occurred
 */
const getRoomByRoomName = async (roomName) => {
    console.log("Getting room with name " + roomName + "...")
    let result = await api.get('rooms.info', {
        roomName: roomName,
    })

    if (result && result.success) {
        return result.room
    } else {
        console.error(result)
        throw new StaffError("Could not find #" + roomName)
    }
}

module.exports = getRoomByRoomName