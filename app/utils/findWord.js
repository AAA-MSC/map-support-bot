const { PublicError } = require("./errors")
const readJSON = require("./readJSON")

const defFile = 'app/utils/definitions.json'


const findWord = (query) => {
  query = query.trim().toLowerCase().replaceAll("-", " ").replaceAll("/", "")

  let definitions
  try {
    definitions = readJSON(defFile)
  } catch (err) {
    console.error(err)
    throw new PublicError("I couldn't access the list of definitions. Staff probably need to fix me.")
  }

  // Find the word based on key or alias if that fails
  if (definitions[query]) {
    definitions[query]["key"] = query
    console.log(definitions[query])
    return definitions[query]
  } else {
    for (term in definitions) {
      for (alias of definitions[term].aliases) {
        if (alias === query) {
          definitions[term]["key"] = term
          return definitions[term]
        }
      }
    }
    throw new PublicError("I can't find a term with the key or alias '" + query + "'!")
  }
}

module.exports = findWord