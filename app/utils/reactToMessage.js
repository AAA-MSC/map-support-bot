const { api } = require('@rocket.chat/sdk')

const reactToMessage = async (emoji, message, shouldReact = true) => {
  try {
    let result = await api.post('chat.react', {
      emoji,
      messageId: message._id,
      shouldReact,
    })
    if (result && result.success) {
      return result.role
    } else {
      console.error(result)
    }
  } catch (err) {
    console.error(err)
    return err
  }
}

module.exports = reactToMessage
