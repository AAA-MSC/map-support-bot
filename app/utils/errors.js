class PublicError extends Error {
    /**
     * Make a command fail with an error message that is visible to users in the channel where the command was called.
     * @param {string} message - The error message that will be displayed to users
     */
    constructor(message) {
        super(message);
        this.name = "PublicError";
    }
}

class StaffError extends Error {
    /**
     * Make a command fail with an error message that is posted to a staff log channel. Users will see a generic error message.
     * @param {string} message - The error message that will be displayed to staff
     */
    constructor(message) {
        super(message);
        this.name = "StaffError";
    }
}

module.exports = {
    PublicError, StaffError
}