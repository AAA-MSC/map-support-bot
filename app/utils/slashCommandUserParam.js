const { api } = require('@rocket.chat/sdk')

/**
 * Runs a slash command that takes a single user as input in a specified channel.
 * @param {string} command - The name of the slash command to run
 * @param {import('@rocket.chat/sdk/dist/utils/interfaces').IRoomAPI} room - The room where the command should be run
 * @param {import('@rocket.chat/sdk/dist/utils/interfaces').IUserAPI} user - The user that the command should affect
 * @returns {boolean} Whether or not the command ran successfully
 */
const slashCommandUserParam = async (command, room, user) => {
  console.log("Running slash command '" + command + "' in #" + room.name + "...")
  let result = await api.post('commands.run', {
    command: command,
    roomId: room.rid ?? room._id,
    params: "@" + user.username
  })

  if (result && result.success) {
    return true
  } else {
    console.error(result)
    return false
  }
}

module.exports = slashCommandUserParam