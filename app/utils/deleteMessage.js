const { api } = require('@rocket.chat/sdk')
const { StaffError } = require('./errors')

/**
 * Deletes a message. Before deleting, the message will be edited to contain only ".". This ensures that the information is hidden immediately for mobile users, since deletes do not sync until local cache is cleared on mobile.
 * @param {string} roomId - The ID of the room containing the message
 * @param {string} msgId - The ID of the message being deleted
 * @param {boolean} asUser - Whether or not the action as the user who originally posted the message. Defaults to false and is not typically necessary.
 * @returns {import('@rocket.chat/sdk/dist/utils/interfaces').IMessageAPI} The deleted message
 * @throws Throws an error if the message could not be exited or an API error occurred
 */
const deleteMessage = async (roomId, msgId, asUser = false) => {
  let result
  let failed = false
  console.log("Deleting message with ID '" + msgId + "' in room with ID '" + roomId + "'...")
  // As long as this succeeds, the message is hidden from users
  result = await api.post('chat.update', { roomId, msgId, text: "." }) // Hide for mobile users
  if (!result || !result.success) {
    failed = true
  }

  // Fully delete the message (will not be reflected immediately on mobile)
  result = await api.post('chat.delete', { roomId, msgId, asUser })
  if (failed) {
    throw new StaffError("Failed to delete message with ID " + msgId + " in room with ID " + roomId)
  }
  if (result && result.success) {
    return result.message
  } else {
    console.error(result)
  }
}

module.exports = deleteMessage
