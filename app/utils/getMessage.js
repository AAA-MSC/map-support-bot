const { api } = require('@rocket.chat/sdk')
const { PublicError } = require('./errors')

/**
 * Get the message associated with a specific message ID.
 * @param {string} msgId - The ID of the message. This can be obtained by clicking "Copy link" in the 3-dot menu of the message and looking for the `msg=` section of the link.
 * @returns {import('@rocket.chat/sdk/dist/utils/interfaces').IMessageReceiptAPI} The message with the ID passed into the util.
 */
const getMessage = async (msgId) => {
  let result = await api.get('chat.getMessage', {
    msgId: msgId,
  })

  if (result && result.success) {
    return result.message
  } else {
    console.error(result)
    throw new PublicError("Could not find message with ID " + msgId)
  }
}

module.exports = getMessage
