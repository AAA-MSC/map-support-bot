const { api } = require('@rocket.chat/sdk')
const { PublicError } = require("./errors.js")

/**
 * Provides information about a user.
 * @param {string} username - The username of the desired user
 * @param {boolean} rooms - Whether or not to include a list of rooms the user is in
 * @returns {import('@rocket.chat/sdk/dist/utils/interfaces').IUserAPI} The user object associated with the username passed in
 * @throws Throws an error if a user with the specified username could not be found or an API error occurred
 */
const getUserByUsername = async (username, rooms = false) => {
  console.log("Getting user with username " + username + "...")
  let result;
  if (rooms) {
    result = await api.get('users.info', {
      username,
      fields: { userRooms: 1 },
    })
  } else result = await api.get('users.info', { username })

  //The SDK library for an invalid user (or other assorted error), will actually throw an exception internally and the promise will return as null.
  //TODO: Revisit with the latest versions of RC and SDK library
  if (result && result.success) {
    return result.user
  } else {
    console.error(result)
    let disallowed = ['all', 'here']
    if (disallowed.includes(username)) username = "that user"
    else username = "@" + username
    throw new PublicError("I couldn't find " + username + "\nCheck your spelling and capitalization")
  }
}

module.exports = getUserByUsername