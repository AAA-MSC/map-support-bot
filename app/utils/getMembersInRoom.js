const { api } = require('@rocket.chat/sdk')

const getMembersInRoom = async (object) => {
  try {
    let result = await api.get('channels.members', object)
    if (result && result.success) {
      return result
    } else {
      console.error(result)
    }
  } catch (err) {
    console.error(err)
    return err
  }
}

module.exports = getMembersInRoom
