const { api } = require('@rocket.chat/sdk')

const upload = async (roomId, options = undefined) => {
  try {
    let result = await api.get(`rooms.upload`, { roomId, ...options })

    console.log(result)
    if (result && result.success) {
      return result
    } else {
      console.error(result)
    }
  } catch (err) {
    console.error(err)
    return err
  }
}

module.exports = upload
