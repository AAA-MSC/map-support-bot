const getRoomByRoomName = require('./getRoomByRoomName')
const { api } = require('@rocket.chat/sdk')
const { StaffError } = require('./errors')
const getUserByUsername = require('./getUserByUsername')
/**
 * Invite user to a list of rooms by their names.
 * @summary Invite user to a list of rooms by the names of the rooms
 * @param {import('@rocket.chat/sdk/dist/utils/interfaces').IUserAPI} user - The user to be invited to the rooms
 * @param {string[]} roomNames - A list of room names (labeled "Name" in the output of the roomInfo dev command)
 * @returns {string[]} A list of failures that occurred while the function was running
 */
const inviteUserByRoomNames = async (user, roomNames) => {
  // Find out which rooms the user is in
  if (!user.rooms) {
    user = await getUserByUsername(user.username, true)
  }
  userRooms = new Set(user.rooms)

  let errors = []
  for (let roomName of roomNames) {
    if (userRooms.has(roomName)) {
      console.log(`Skipped adding ${user.name} to ${roomName} since they're already in it.`)
    } else {
      try {
        let room = await getRoomByRoomName(roomName)
        await inviteUserToRoom(user, room)
      } catch (err) {
        console.error(err)
        errors.push(`Error while inviting ${user.name} to #${roomName}: ${err.message}`)
      }
    }
  }
  return errors
}

/**
 * Adds a user to a room using a room object.
 * @param {import('@rocket.chat/sdk/dist/utils/interfaces').IUserAPI} user - The being added t the room
 * @param {import('@rocket.chat/sdk/dist/utils/interfaces').IRoomAPI} room - The room that the user is being added to
 * @returns {import('@rocket.chat/sdk/dist/utils/interfaces').IRoomAPI} The room that the user was added to
 * @throws Throws an error if the user could not be invited to a room or an API error occurred
 */
const inviteUserToRoom = async (user, room) => {
  if (!room) throw new StaffError("Room is not defined")
  console.log("Inviting " + user.name + " to " + room.name + "...")
  let roomId = room.rid ?? room._id
  let result
  if (room.t === 'c') {
    result = await api.post('channels.invite', {
      roomId: roomId,
      userId: user._id,
    })
  } else if (room.t === 'p') {
    result = await api.post('groups.invite', {
      roomId: roomId,
      userId: user._id,
    })
  } else {
    throw new StaffError("#" + room.name + " has invalid type " + room.t)
  }
  if (result && result.success) {
    return result.channel ?? result.group
  } else {
    console.error(result)
    throw new StaffError("Failed to invite " + user.name + " to #" + room.name)
  }
}

module.exports = inviteUserByRoomNames