const userHasRole = require('./userHasRole')
const userHasRoles = require('./userHasRoles')

/**
 * Checks that the target of a command is a subordinate of the person calling the command. This should not be used in commands that non-staff users can call. By design, no user outranks themself.
 * @param {import('@rocket.chat/sdk/dist/utils/interfaces').IUserAPI} target - The user that the command should affect
 * @param {import('@rocket.chat/sdk/dist/utils/interfaces').IUserAPI} caller - The user calling the command (must be a staff member)
 * @returns {boolean} Whether or not the target user is a subordinate of the caller
 */
const isSubordinate = (target, caller) => {
  console.log("Checking that " + caller.name + " outranks " + target.name + "...")
  if (!userHasRoles(caller, ['Guide', 'Global Moderator', 'admin'])) {
    return false // Normal users don't outrank anyone
  }
  if (userHasRole(target, 'admin')) { return false } // Nobody outranks admins
  else if (userHasRole(target, 'Global Moderator')) {
    return userHasRoles(caller, ['admin']) // Only admins outrank mods
  } else if (userHasRole(target, 'Guide')) {
    return userHasRoles(caller, ['admin', 'Global Moderator']) // Admins and mods outrank guides
  } else return true // All staff members outrank normal users
}

module.exports = isSubordinate