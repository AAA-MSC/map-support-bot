#!/usr/bin/node /var/www/msc_bot/app/index.js

require('dotenv').config()

const { driver } = require('@rocket.chat/sdk')

const parseCommandArguments = require('./utils/parseCommandArguments')
const getUserByUsername = require('./utils/getUserByUsername')
const userHasRoles = require('./utils/userHasRoles')

const { PublicError, StaffError } = require('./utils/errors.js')

// Import commands
const adminCommands = require('./commands/admins')
const moderatorsCommands = require('./commands/moderators')
const developerCommands = require('./commands/developer')
const guideCommands = require('./commands/guides')
const memberCommands = require('./commands/members')

const help = require('./commands/help')

// customize the following with your server and BOT account information
const HOST = process.env.ROCKETCHAT_URL
const USER = process.env.ROCKETCHAT_USER
const PASS = process.env.ROCKETCHAT_PASSWORD
const BOTNAME = process.env.ROCKETCHAT_BOTNAME
const SSL = process.env.ROCKETCHAT_USE_SSL === 'true' ? true : false

const PREFIX = process.env.ROCKETCHAT_PREFIX
const IGNORE_OWN_MESSAGES =
  process.env.ROCKETCHAT_IGNORE_OWN_MESSAGES === 'true' ? true : false
const LOG_CHANNEL_ID = process.env.LOG_CHANNEL_ID


console.log(HOST, USER, PASS, SSL, BOTNAME)

let userId

const commands = {
  ...adminCommands.commands,
  ...moderatorsCommands.commands,
  ...guideCommands.commands,
  ...developerCommands.commands,
  ...memberCommands.commands,
}

console.log(commands)

let cooldowns = {}

const runBot = async () => {
  const connection = await driver.connect(
    { host: HOST, useSsl: SSL },
    (error, args) => {
      if (error) {
        console.log(error)
      } else {
        console.log('Connected', args)
      }
    },
  )
  userId = await driver.login({ username: USER, password: PASS })

  // Set up subscriptions - rooms we're interested in listening to
  const subscribed = await driver.subscribeToMessages()
  console.log('subscribed')

  if (PREFIX === '!map') {
    // Connect the processMessages callback
    driver.respondToMessages(processMessages, {
      rooms: true,
      dm: true,
      edited: false,
    })
  } else {
    const msgLoop = await driver.reactToMessages(processMessages)
  }

  console.log('connected and waiting for messages')
  await driver.sendToRoom("MSC bot started", LOG_CHANNEL_ID)
}

// Callback for incoming messages filter and processing
const processMessages = async (error, message, messageOptions) => {
  console.log(message)

  let messageText = message.msg.toLowerCase().trim()

  if (!messageText.startsWith(PREFIX)) {
    return
  }

  // Fold messageOptions into message
  message = {
    ...message,
    ...messageOptions,
  }

  if (messageText === PREFIX) {
    // Special case for no command
    const roomID = message.rid

    await driver.sendToRoom("I'm MAP Support Bot, MSC's friendly helper bot. To learn more about what I can do, send the message `" + PREFIX + " help`", roomID)
    return
  }

  if (!error) {
    // Special case for the help command
    if (messageText.startsWith(`${PREFIX} help`)) {
      // Get the arguments
      let argumentList = parseCommandArguments(
        message.msg.toLowerCase().substr(`${PREFIX} help `.length),
      )

      let context = {
        commandName: 'help',
        prefix: PREFIX,
        commands,
        argumentList,
      }

      // Call the command function
      help({
        bot: driver,
        message,
        context,
      })
      return
    }

    // Check if the message is a command
    let foundCommand = false
    for (let commandGroup of Object.keys(commands)) {
      for (let commandName of Object.keys(commands[commandGroup])) {
        let command = commands[commandGroup][commandName]

        // Check if this is the command the user called
        if (messageText.startsWith(`${PREFIX} ${commandName.toLowerCase()}`)) {
          foundCommand = true
          console.log(commandGroup, command)
          // Check if message author has one of the required roles
          if (
            // Empty list? or...
            command.requireOneOfRoles == false ||
            // If not, then Undefined list? or...
            command.requireOneOfRoles === undefined ||
            // If not, then does user have one of the roles?
            userHasRoles(
              await getUserByUsername(message.u.username),
              command.requireOneOfRoles,
            )
          ) {
            const currentDate = new Date()
            const cooldown = command.cooldown

            // Check if the command has a cooldown
            if (command.cooldown) {
              // Check that the cooldown is an integer
              if (!(cooldown === parseInt(cooldown, 10))) {
                console.error(
                  `\n\n\n COOLDOWN FOR COMMAND ${command} IS NOT AN INTEGER \n\n\n`,
                )
                return
              }

              // Check if the user has any cooldowns and if they have a cooldown for the command
              if (
                cooldowns[message.u._id] &&
                cooldowns[message.u._id][commandName]
              ) {
                // Check if the required amount of time has passed
                const userCooldown = cooldowns[message.u._id][commandName]

                if (currentDate.getTime() >= userCooldown.getTime()) {
                  // Cooldown time has passed; proceed with calling the command
                } else {
                  // If the necessary amount of time hasn't passed,
                  // tell the user that the command is in cooldown
                  await driver.sendToRoom(
                    `The command is in cooldown! ⏱ Try again in ${Math.abs(
                      (currentDate.getTime() - userCooldown.getTime()) / 1000,
                    )} seconds.`,
                    message.rid,
                  )
                  // And stop calling the command
                  return
                }
              }
            }

            // Get the arguments
            let argumentList = parseCommandArguments(
              message.msg.substr(`${PREFIX} ${commandName} `.length),
            )

            // Create the context object
            let context = {
              ...command,
              commandName,
              prefix: PREFIX,
              staffLog: LOG_CHANNEL_ID,
              argumentList,
            }

            let commandSuccess
            try {
              // Call the command function
              commandSuccess = await command.call({
                bot: driver,
                message,
                context,
              })
            } catch (error) {
              console.error(error)
              if (error instanceof PublicError) await driver.sendToRoom(error.message, message.rid)
              else {
                if (message.rid !== LOG_CHANNEL_ID) await driver.sendToRoom('Command error', message.rid)
                if (error instanceof StaffError) await driver.sendToRoom(error.message, LOG_CHANNEL_ID)
              }
              commandSuccess = false
            }
            if (command.cooldown && (commandSuccess == true || commandSuccess == null)) {
              if (
                cooldowns[message.u._id] &&
                cooldowns[message.u._id][commandName]
              ) {
                cooldowns[message.u._id][commandName] = new Date(
                  currentDate.getTime() + cooldown * 1000,
                )
              } else {
                // If the user does not have a cooldown for the command,
                // give the user a cooldown for the command,
                cooldowns[message.u._id] = {
                  ...cooldowns[message.u._id],
                  [commandName]: new Date(
                    currentDate.getTime() + cooldown * 1000,
                  ),
                }
              }
            }
          } else {
            // Message author doesn't have the required roles, nothing else to do
            const response =
              "Sorry, you don't have one of the required roles 😶"
            const sentMsg = await driver.sendToRoom(response, message.rid)
            return
          }
          break // Break the for loop
        }
      }
    }
    if (!foundCommand) {
      await driver.sendToRoom("Sorry, I couldn't find that command. Run `" + PREFIX + " help` to see available commands.", message.rid)
    }
  } else if (error) {
    console.error(error)
  }
}

runBot()
